%% Empirical Methods hw1 by Jinyoung Seo
%% Q1
X=[1,1.5,3,4,5,7,9,10];
Y1=-2+0.5*X;
Y2=-2+0.5*X.^2;

plot(X,[Y1;Y2])

%% Q2
x=linspace(-10,20,200)';
sum(x);

%% Q3
A=[2 4 6;1 7 5;3 12 4];
B=[-2;3;10];
C=A'*B;
% inv is slow. use \ operator as in answer key.
D=inv(A'*A)*B;
E=0;
for i=1:3
    for j=1:3
        E=E+A(i,j)*B(i);
    end
end
F= A([1 3],[1 2]);

x=A\B;

%% Q4
% check the use of kron()
B=blkdiag(A,A,A,A,A);

%% Q5
A=5*randn(5,3)+10;
% easier to do A = A>10
for i=1:5
    for j=1:3
        if A(i,j)<10
            A(i,j)=0;
        else
            A(i,j)=1;
        end
    end
end
%% Q6

load('datahw1.csv');

y=datahw1(:,5);n=max(size(y));
X=[ones(size(y)) datahw1(:,3), datahw1(:,4) datahw1(:,6)];
% again, inv is slow, use \
beta= inv(X'*X)*X'*y;
e= y-X*beta;
std_error=e'*e*inv(X'*X)/(n-4);



for i=1:4
    se(i)=sqrt(std_error(i,i));
    tstats(i)= (beta(i))/(se(i));
    pv(i)=tcdf(tstats(i),n-4,'upper');
end



